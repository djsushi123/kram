package com.gitlab.djsushi123.kram.entity

data class Poll(
    val id: String,
    val question: String,
    val options: List<PollOption>,
    val totalVoterCount: Int,
    val isClosed: Boolean,
    val isAnonymous: Boolean,
    val type: String,
    val allowsMultipleAnswers: Boolean,
    val correctOptionId: Int? = null,
    val explanation: String? = null,
    val explanationEntities: List<MessageEntity>? = null,
    val openPeriod: Int? = null,
    val closeGate: Int? = null
)