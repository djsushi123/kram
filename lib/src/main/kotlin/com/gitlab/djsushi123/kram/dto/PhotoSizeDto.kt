package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.PhotoSize
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class PhotoSizeDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("width") val width: Int,
    @SerialName("height") val height: Int,
    @SerialName("file_size") val fileSize: Int? = null
)

internal fun PhotoSizeDto.toEntity() = PhotoSize(
    fileId = fileId,
    fileUniqueId = fileUniqueId,
    width = width,
    height = height,
    fileSize = fileSize
)

internal fun PhotoSize.toDto() = PhotoSizeDto(
    fileId = fileId,
    fileUniqueId = fileUniqueId,
    width = width,
    height = height,
    fileSize = fileSize
)