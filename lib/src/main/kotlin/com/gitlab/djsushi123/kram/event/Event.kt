package com.gitlab.djsushi123.kram.event

import com.gitlab.djsushi123.kram.KramBot
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.job

interface Event : CoroutineScope

internal fun kramCoroutineScope(bot: KramBot): CoroutineScope =
    CoroutineScope(bot.coroutineContext + SupervisorJob(bot.coroutineContext.job))