package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.VideoNote
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class VideoNoteDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("length") val length: Int,
    @SerialName("duration") val duration: Int,
    @SerialName("thumb") val thumb: PhotoSizeDto? = null,
    @SerialName("file_size") val fileSize: Long? = null
)

internal fun VideoNoteDto.toEntity(): VideoNote {
    return VideoNote(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        length = length,
        duration = duration,
        thumb = thumb?.toEntity(),
        fileSize = fileSize
    )
}

internal fun VideoNote.toDto(): VideoNoteDto {
    return VideoNoteDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        length = length,
        duration = duration,
        thumb = thumb?.toDto(),
        fileSize = fileSize
    )
}