package com.gitlab.djsushi123.kram.entity

data class ReplyKeyboardRemove(
    val removeKeyboard: Boolean,
    val selective: Boolean? = null
) : ReplyMarkup