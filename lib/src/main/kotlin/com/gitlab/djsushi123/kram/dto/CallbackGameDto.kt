package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.CallbackGame
import kotlinx.serialization.Serializable

@Serializable
internal class CallbackGameDto

internal fun CallbackGameDto.toEntity(): CallbackGame {
    return CallbackGame()
}

internal fun CallbackGame.toDto(): CallbackGameDto {
    return CallbackGameDto()
}