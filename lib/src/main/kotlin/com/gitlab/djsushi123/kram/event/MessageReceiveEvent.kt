package com.gitlab.djsushi123.kram.event

import com.gitlab.djsushi123.kram.KramBot
import com.gitlab.djsushi123.kram.entity.message.Message
import kotlinx.coroutines.CoroutineScope

class MessageReceiveEvent(
    private val bot: KramBot,
    val message: Message,
    private val coroutineScope: CoroutineScope = kramCoroutineScope(bot)
) : Event, CoroutineScope by coroutineScope