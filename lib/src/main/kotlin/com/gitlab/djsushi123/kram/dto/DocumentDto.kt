package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.Document
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class DocumentDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("thumb") val thumb: PhotoSizeDto? = null,
    @SerialName("file_name") val fileName: String? = null,
    @SerialName("mime_type") val mimeType: String? = null,
    @SerialName("file_size") val fileSize: Long? = null
)

internal fun DocumentDto.toEntity(): Document {
    return Document(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        thumb = thumb?.toEntity(),
        fileName = fileName,
        mimeType = mimeType,
        fileSize = fileSize
    )
}

internal fun Document.toDto(): DocumentDto {
    return DocumentDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        thumb = thumb?.toDto(),
        fileName = fileName,
        mimeType = mimeType,
        fileSize = fileSize
    )
}