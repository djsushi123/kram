package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.PassportFile
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class PassportFileDto(
    @SerialName("file_id") val fileId: String,
    @SerialName("file_unique_id") val fileUniqueId: String,
    @SerialName("file_size") val fileSize: Long,
    @SerialName("file_date") val fileDate: Int
)

internal fun PassportFileDto.toEntity(): PassportFile {
    return PassportFile(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        fileSize = fileSize,
        fileDate = fileDate
    )
}

internal fun PassportFile.toDto(): PassportFileDto {
    return PassportFileDto(
        fileId = fileId,
        fileUniqueId = fileUniqueId,
        fileSize = fileSize,
        fileDate = fileDate
    )
}