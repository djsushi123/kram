package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.SuccessfulPayment
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class SuccessfulPaymentDto(
    @SerialName("currency") val currency: String,
    @SerialName("total_amount") val totalAmount: Int,
    @SerialName("invoice_payload") val invoicePayload: String,
    @SerialName("shipping_option_id") val shippingOptionId: String? = null,
    @SerialName("order_info") val orderInfo: OrderInfoDto? = null,
    @SerialName("telegram_payment_charge_id") val telegramPaymentChargeId: String,
    @SerialName("provider_payment_charge_id") val providerPaymentChargeId: String
)

internal fun SuccessfulPaymentDto.toEntity(): SuccessfulPayment {
    return SuccessfulPayment(
        currency = currency,
        totalAmount = totalAmount,
        invoicePayload = invoicePayload,
        shippingOptionId = shippingOptionId,
        orderInfo = orderInfo?.toEntity(),
        telegramPaymentChargeId = telegramPaymentChargeId,
        providerPaymentChargeId = providerPaymentChargeId
    )
}

internal fun SuccessfulPayment.toDto(): SuccessfulPaymentDto {
    return SuccessfulPaymentDto(
        currency = currency,
        totalAmount = totalAmount,
        invoicePayload = invoicePayload,
        shippingOptionId = shippingOptionId,
        orderInfo = orderInfo?.toDto(),
        telegramPaymentChargeId = telegramPaymentChargeId,
        providerPaymentChargeId = providerPaymentChargeId
    )
}