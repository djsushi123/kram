package com.gitlab.djsushi123.kram.entity

data class ForceReply(
    val forceReply: Boolean,
    val inputFieldPlaceholder: String? = null,
    val selective: Boolean? = null
) : ReplyMarkup