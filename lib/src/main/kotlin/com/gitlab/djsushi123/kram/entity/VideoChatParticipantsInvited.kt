package com.gitlab.djsushi123.kram.entity

data class VideoChatParticipantsInvited(
    val users: List<User>
)