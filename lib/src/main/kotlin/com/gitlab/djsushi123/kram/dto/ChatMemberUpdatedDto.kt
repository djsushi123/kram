package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.ChatMemberUpdated
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ChatMemberUpdatedDto(
    @SerialName("chat") val chat: ChatDto,
    @SerialName("from") val from: UserDto,
    @SerialName("date") val date: Int,
    @SerialName("old_chat_member") val oldChatMember: ChatMemberDto,
    @SerialName("new_chat_member") val newChatMember: ChatMemberDto,
    @SerialName("invite_link") val inviteLink: ChatInviteLinkDto? = null
)

internal fun ChatMemberUpdatedDto.toEntity(): ChatMemberUpdated {
    return ChatMemberUpdated(
        chat = chat.toEntity(),
        from = from.toEntity(),
        date = date,
        oldChatMember = oldChatMember.toEntity(),
        newChatMember = newChatMember.toEntity(),
        inviteLink = inviteLink?.toEntity()
    )
}

internal fun ChatMemberUpdated.toDto(): ChatMemberUpdatedDto {
    return ChatMemberUpdatedDto(
        chat = chat.toDto(),
        from = from.toDto(),
        date = date,
        oldChatMember = oldChatMember.toDto(),
        newChatMember = newChatMember.toDto(),
        inviteLink = inviteLink?.toDto()
    )
}