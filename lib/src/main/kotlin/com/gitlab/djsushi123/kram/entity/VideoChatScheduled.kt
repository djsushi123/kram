package com.gitlab.djsushi123.kram.entity

data class VideoChatScheduled(
    val startDate: Int
)