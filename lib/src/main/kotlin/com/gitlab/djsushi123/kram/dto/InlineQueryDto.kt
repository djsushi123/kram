package com.gitlab.djsushi123.kram.dto

import com.gitlab.djsushi123.kram.entity.InlineQuery
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class InlineQueryDto(
    @SerialName("id") val id: String,
    @SerialName("from") val from: UserDto,
    @SerialName("query") val query: String,
    @SerialName("offset") val offset: String,
    @SerialName("chat_type") val chatType: String? = null,
    @SerialName("location") val location: LocationDto? = null
)

internal fun InlineQueryDto.toEntity(): InlineQuery {
    return InlineQuery(
        id = id,
        from = from.toEntity(),
        query = query,
        offset = offset,
        chatType = chatType,
        location = location?.toEntity()
    )
}

internal fun InlineQuery.toDto(): InlineQueryDto {
    return InlineQueryDto(
        id = id,
        from = from.toDto(),
        query = query,
        offset = offset,
        chatType = chatType,
        location = location?.toDto()
    )
}